<?php

namespace App\Repository;

use App\Entity\HohohoRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HohohoRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method HohohoRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method HohohoRating[]    findAll()
 * @method HohohoRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HohohoRatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HohohoRating::class);
    }

    // /**
    //  * @return HohohoRating[] Returns an array of HohohoRating objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HohohoRating
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
