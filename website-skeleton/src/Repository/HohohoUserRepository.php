<?php

namespace App\Repository;

use App\Entity\HohohoUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HohohoUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method HohohoUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method HohohoUser[]    findAll()
 * @method HohohoUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HohohoUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HohohoUser::class);
    }

    // /**
    //  * @return HohohoUser[] Returns an array of HohohoUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HohohoUser
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
