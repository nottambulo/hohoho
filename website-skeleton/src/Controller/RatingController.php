<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/rating", name="rating")
 */
class RatingController extends AbstractController
{
    const M_STATUSES = ['Йа КреведКО', 'Tolya_Kolokol', 'ЧеЛоВеГгг', '}{уЛиGaN ', '˜”*°•.Крылья◐ С۞ветов◐ Чемпи☼н.•°*”˜', '✯Сержант диванных битв✯', 'Гѻрдӹй★ПᎮúнц ', 'ĎERѮЌЇЙ', 'Гϼѐшԋúк', '✯✯HагиБатоР 2000✯✯'];
    const F_STATUSES = ['ツ_Улыбка_ツ', '°*”˜˜”*°•.Сладкий_KiSs.•°*”˜˜”*°', '_БеГуЩаЯ_пО_гРаБлЯм_', 'Ҹёፐкȧя Мандმрūԋҟმ', 'ЦелуЙ эКраН поКа oN-LIne ', '✯✯]{o№ƒet()4]{@✯✯', 'мЕго_О-цЫпа==>>', '%%П_у_Ф_ф_А_$-т_и_К%%', '▀▄ Багирра▀▄', '♪ ♪ ♪ДевYшка_8не_закон@ ♪ ♪ ♪ '];

    /**
     * @Route("/", name="rating:")
     */
    public function index()
    {
        $stat = $this->getStat($this->getUser()->getRating(), $this->getUser()->getGender());

        return $this->render('rating/index.html.twig', [
            'user' => $this->getUser(),
            'status' => $stat['status'],
            'level' => $stat['level']
        ]);
    }

    /**
     * @Route("/add", name="rating:add", methods={"POST"})
     */
    public function addRating(): Response
    {
        $this->getUser()->setRating($this->getUser()->getRating() + 200);
        return new Response('ok');
    }

    /**
     * @param $rating
     * @param $gender
     *
     * @return array
     */
    public function getStat($rating, $gender): array
    {
        $lig = self::M_STATUSES;
        if (strtoupper($gender) == 'F') {
            $lig = self::F_STATUSES;
        }

        if ($rating <= 200) {
            $res = ['level' => 1, 'status' => $lig[0]];
        } elseif ($rating <= 400) {
            $res = ['level' => 2, 'status' => $lig[1]];
        } elseif ($rating <= 600) {
            $res = ['level' => 3, 'status' => $lig[2]];
        } elseif ($rating <= 800) {
            $res = ['level' => 4, 'status' => $lig[3]];
        } elseif ($rating <= 1000) {
            $res = ['level' => 5, 'status' => $lig[4]];
        } elseif ($rating <= 1200) {
            $res = ['level' => 6, 'status' => $lig[5]];
        } elseif ($rating <= 1400) {
            $res = ['level' => 7, 'status' => $lig[6]];
        } elseif ($rating <= 1600) {
            $res = ['level' => 8, 'status' => $lig[7]];
        } elseif ($rating <= 1800) {
            $res = ['level' => 9, 'status' => $lig[8]];
        } else {
            $res = ['level' => 10, 'status' => $lig[9]];
        }

        return $res;
    }

}
