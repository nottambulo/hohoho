<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\HohohoUserRepository;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{


    /**
     * Subscribe user
     *
     * @param Request $request
     *
     * @Route("/subscribe", name="user:subscribe")
     *
     * @return Response
     */
    public function subscribe(Request $request): Response
    {
        $email = trim(mb_strtolower($request->get('email')));

        $oUser = new User();
        $oUser->setEmail($email);
        $oUser->setRegIp($request->getClientIp());

        $em = $this->get('doctrine.orm.entity_manager');
        $em->persist($oUser);
        $em->flush();

        $this->login($email, $this->getDoctrine()->getRepository(User::class));
        return new Response('ok');
    }


    /**
     * @Route("/login", name="user:login")
     */
    public function ulogin( ){
        return $this->render('user/login.html.twig', [
            'controller_name' => 'GameController',
        ]);
    }

    protected function login(string $email, HohohoUserRepository $userRepository): bool
    {
        $user = $userRepository->findOneBy(['email' => $email]);
        if (null == $user) {
            return false;
        }

        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        if (empty($token)) {
            return false;
        }

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));

        return true;
    }


    public function exist(Request $request, HohohoUserRepository $repository): Response
    {
        $email = trim(strtolower($request->get('email')));
        if (empty($email)) {
            return new Response('true');
        }

        if ($this->_isValidMx($email)) {
            return new Response('true');
        }

        if ($repository->findOneBy(['email' => $email])) {
            return new Response('true');
        }

        return new Response('false');
    }


    /**
     * Check valid email
     *
     * @param string $email (email)
     *
     * @return bool
     */
    private function _isValidMx(string $email): bool
    {
        $hostData = explode('@', $email);
        $host = $hostData[1];
        $arrMxHosts = [];
        $mx = getmxrr($host, $arrMxHosts);
        $err = true;
        if ($mx) {
            $err = false;
        }
        foreach ($arrMxHosts as $mxHost) {
            if (is_null($mxHost)) {
                $err = true;
                continue;
            }
            if (preg_match('/[0]\.[0]\.[0]\.[0]/', $mxHost)) {
                $err = true;
                continue;
            }
            if (preg_match('/wildcard*.?\./', $mxHost)) {
                $err = true;
                continue;
            }
            $err = false;
        }
        return $err;
    }

}
