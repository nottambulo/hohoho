<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class GameController extends AbstractController
{
    /**
     * @Route("/game", name="game")
     */
    public function index()
    {
        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
        ]);
    }


    /**
     * @Route("/reg-colleague", name="game:reg_colleague")
     */
    public function reg_colleague(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        switch ($request->getMethod()) {
            case 'GET':
                return $this->render('game/reg_colleague.html.twig');
                break;
            case 'POST':

                $this->getUser()->setRating($this->getUser()->getRating() + 200);

                break;
        }
    }

    /**
     * @Route("/sea-battle", name="rating")
     */
    public function seaBattle()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('game/sea_battle.html.twig');
    }



}
